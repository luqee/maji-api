from maji import db

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64))
    number = db.Column(db.String(15), index=True, unique=True)

    def __init__(self, username, number):
        self.username = username
        self.number = number
    
    def save(self):
        db.session.add(self)
        db.session.commit()

class Provider(db.Model):
    __tablename__ = 'providers'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64))
    number = db.Column(db.String(15), index=True, unique=True)
    status = db.Column(db.String(64))
    latitude = db.Column(db.String(65))
    longitude = db.Column(db.String(65))

    def __init__(self, username, number, latitude, longitude, status='Available',):
        self.username = username
        self.number = number
        self.status = status
        self.latitude = latitude
        self.longitude = longitude
    
    def save(self):
        db.session.add(self)
        db.session.commit()

class Transaction(db.Model):
    __tablename__ = 'transactions'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    provider_id = db.Column(db.Integer, db.ForeignKey('providers.id'))
    # start_time = db.Column(db.DateTime)
    # finish_time = db.Column(db.DateTime)
    # service_desription = db.Column(db.String)
    # service_cost = db.Column(db.Integer)

    def __init__(self, user_id, provider_id):
        self.user_id = user_id
        self.provider_id = provider_id
    
    def save(self):
        db.session.add(self)
        db.session.commit()

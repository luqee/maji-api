import json
# import requests
from maji import app, models, db
from flask import request, abort

@app.route('/maji/api/v1.0/auth/user/register', methods=['POST'])
def register_user():
    name = request.form.get('username')
    number = request.form.get('number')
    nuser = models.User(name, number)
    nuser.save()
    mssg = {}
    if nuser.id:
        mssg['result'] = 'success'
        mssg['user_id'] = nuser.id
    else:
        mssg['result'] = 'error'
    return json.dumps(mssg)

@app.route('/maji/api/v1.0/auth/provider/register', methods=['POST'])
def register_provider():
    name = request.form.get('username')
    number = request.form.get('number')
    lat = request.form.get('latitude')
    longitude = request.form.get('longitude')
    provider = models.Provider(name, number, lat, longitude)
    provider.save()
    mssg = {}
    if provider.id:
        mssg['result'] = 'success'
        mssg['provider_id'] = provider.id
        mssg['provider_num'] = provider.number
    else:
        mssg['result'] = 'error'
    return json.dumps(mssg)

@app.route('/maji/api/v1.0/providers/', methods=['GET'])
def get_providers():
    providers = models.Provider.query.filter_by(status='Available').all()
    result = {}
    result['providers'] = []
    for plum in providers:
        pro = {}
        pro['id'] = plum.id
        pro['lat'] = plum.latitude
        pro['long'] = plum.longitude

        result['providers'].append(pro)
    return json.dumps(result)

@app.route('/maji/api/v1.0/users/request', methods=['GET'])
def request_provider():
    if request.method == 'GET':
        user_id = request.args.get('userId')
        selected_provider = models.Provider.query.filter_by(status='Available').first()
        res = {}
        if selected_provider is not None:
            
            transaction = models.Transaction(user_id,selected_provider.id)
            transaction.save()
            if transaction.id:
                res['message'] = 'success'
                res['activated_user'] = {
                    'provider_name': selected_provider.username,
                    'provider_number': selected_provider.number,
                    'provider_id': selected_provider.id,
                    'transaction_id': transaction.id
                }
                #update provider status to 'Engaged'
                selected_provider.status = 'Engaged'
                db.session.commit()

                # user = models.User.query.get(user_id)
                # url = 'https://fcm.googleapis.com/fcm/send'
                # headers = {
                #     'Authorization': 'key='+'AAAAxvIgyUU:APA91bFxvkmWVUQf0pnNrwk8XprlJDqPXNvWUGN72vwEoJO7mt0pFguCfyC8NM3K9QWxnC9ojHJtFHbEoWKyh5vR_RijZjIN7uz7hi56MZTJVLyqBHyZaUhJoelf6hrWqZl5kiZm6xPi',
                #     'Content-Type': 'application/json'
                # }
                # message = {
                    
                #     'data': {
                #         'message_type': 'user_request',
                #         'user_latitude': user.latitude,
                #         'user_longitude': user.longitude,
                #         'transaction_id': transaction.id
                #     },
                #     'to': selected_provider.registration_token
                # }
                # #send push notification to provider
                # r = requests.post(url, headers=headers, json=message)
                return json.dumps(res)
        else:
            res['message'] = 'fail'
            return json.dumps(res)
    else:
        return abort(404)

@app.route('/maji/api/v1.0/auth/provider/changeStatus', methods=['POST'])
def update_status():
    print(request.form.get('status'))
    u_id = request.form.get('user_id')
    models.Provider.query.filter_by(id= u_id).update({
        'status': request.form.get('status')
    })
    db.session.commit()
    result = {}
    result['message'] = 'Success'
    stats = models.Provider.query.get(id=u_id).status
    print(stats)
    result['current_status'] = stats
    return json.dumps(result)
from maji import db, models

db.create_all()

user1 = models.Provider('sketch', '9340598345', '-1.26434635', '36.90401051')
user2 = models.Provider('lucy', '4353523435', '-1.27298236', '36.9093448')
user3 = models.Provider('mike', '435345234', '-1.26350837', '36.91252026')
user4 = models.Provider('fred', '45465642', '-1.26123144', '36.9111348')
user5 = models.Provider('ras', '2544779867', '-1.27505358', '36.91655707')
user6 = models.Provider('frank', '3243535', '-1.26479672', '36.90823027')
user7 = models.Provider('ben', '23423', '-1.26757543', '36.91819788')
user8 = models.Provider('brayo', '345345436', '-1.26508302', '36.91660733')
user9 = models.Provider('shiko', '32435', '-1.27090734', '36.90516374')
user10 = models.Provider('joy', '5465645', '-1.26811112', '36.9208214')
user11 = models.Provider('mary', '235436', '-1.26970079', '36.90772166')

db.session.add(user1)
db.session.add(user2)
db.session.add(user3)
db.session.add(user4)
db.session.add(user5)
db.session.add(user6)
db.session.add(user7)
db.session.add(user8)
db.session.add(user9)
db.session.add(user10)
db.session.add(user11)

db.session.commit()